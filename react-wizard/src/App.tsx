import * as React from 'react';
import './App.css';
import Description from './components/Description/Description';
import Header from './components/Header/Header';
import logo from './logo.svg';
import WizardSteps from './components/WizardSteps/WizardSteps';
import { Login } from './components/Login/Login'
class App extends React.Component {
  public render() {
    return (
      <div className="App">
        {/* <header className="App-header"> */}
        <header className="">
          <img src={logo} className="App-logo" alt="logo" />
          <Header name="REACT" />
        </header>
        <Login />
        {/* <Description countBy={5} />
        <WizardSteps nextPage={1} /> */}
      </div>
    );
  }
}

export default App;