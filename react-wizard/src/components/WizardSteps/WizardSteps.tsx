import * as React from 'react';
import WizardPage from './WizardPage'

interface IProps {
    nextPage? : number
} 

interface IState {
    page: number;
}

class WizardSteps extends React.Component<IProps, IState>{
    public static defaultProps: Partial<IProps> = {
        nextPage: 1
    };

    public state: IState = {
        page: 0,
    };

    public nextPage = () => {
        const pageby: number = this.props.nextPage!;
        const page = this.state.page + pageby;
        this.setState({ page }); 
    }

    public previousPage = () => {
        const pageby: number = this.props.nextPage!;
        const page = this.state.page - pageby;
        this.setState({ page }); 
    }
   
    public render() 
    {
        let title = "Title:"+ this.state.page
        let description = "Descripiton: " + this.state.page
        return (
            <div>
                <button onClick={this.nextPage}>Next Page</button>
                <button onClick={this.previousPage}>Previous Page</button>
                <WizardPage pageNumber={this.state.page} />
            </div>
        )
    }
}

export default WizardSteps;