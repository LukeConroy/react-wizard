import * as React from 'react';

interface IProps {
    pageNumber: number
} 

interface IState {
    title: string,
    description: string
}

class WizardPage extends React.Component<IProps,IState>{
    public static defaultProps: Partial<IProps> = {
        pageNumber: 0
    };

    public state: IState = {
        title: 'This is the title of no.',
        description: 'This is the description of no.'
        
    };

    public render() {
        let title = this.state.title + this.props.pageNumber
        let description = this.state.description + this.props.pageNumber
        return (
            <div>
                <div>{title}</div>
                <div>{description}</div>
            </div>
        )
    }
}

export default WizardPage;