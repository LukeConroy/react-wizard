import ReactDOM from 'react-dom';
import * as React from 'react';
import { GoogleLogin , GoogleLogout} from 'react-google-login';
import configData from "../../config.json";

interface IState {
  userIsLoggedIn: boolean;
}

export class GoogleLoginChoice extends React.Component {

    responseGoogle = (response: any) => {
      console.log(response);
    }

    logout = () => {
      console.log('logout') // eslint-disable-line
      this.setState({ userIsLoggedIn : false });
    }

    success = (response: any) => {
      console.log(response) // eslint-disable-line
      this.setState({ userIsLoggedIn : true });
    }

    error = (response: any) => {
      console.error(response) // eslint-disable-line
    }
    
    public state: IState = {
      userIsLoggedIn: false,
    };
    
    MountTest = () => {
        return (
          <GoogleLogin
            isSignedIn={true}
            theme="dark" 
            onSuccess={res => {
              // toggleShow(false)
              this.success(res)
            }}
            onFailure={this.error}
            clientId={ configData.GOGGLE_CLIENT_ID }
          >
            Login with Google
          </GoogleLogin>
        )
    }
    
    AlreadyLoggedIn = () => {
      return  (
          <GoogleLogout
              clientId={configData.GOGGLE_CLIENT_ID}
              buttonText="Logout"
              onLogoutSuccess={this.logout}
          >
          </GoogleLogout>
          )
    }

    render(): JSX.Element {
        return ( 
        <div>
          { (this.state.userIsLoggedIn) == false ? <this.MountTest /> : <this.AlreadyLoggedIn />   }
        </div>
        )
    }
 
}